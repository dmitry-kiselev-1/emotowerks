﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Emw.EnergyBids.Contracts
{
    public interface IBidsStorage
    {
        Task<IEnumerable<Bid>> GetBidsAsync(DateTime startTimeUtc, DateTime endTimeUtc, Guid? energyGroupId);

        Task<Bid> GetBidByIdAsync(Guid bidId);

        Task CreateBidAsync(Bid newBid);

        Task UpdateBidAsync(Bid bid);
    }
}
