﻿using System;

namespace Emw.EnergyBids.Contracts
{
    public class BidElement
    {
        public BidElement(){}

        public Guid Id { get; set; }

        public Guid EnergyBidId { get; set; }

        public Bid EnergyBid { get; set; }

        public DateTime StartTimeUtc { get; set; }

        public DateTime EndTimeUtc { get; set; }

        public decimal PowerKw { get; set; }
    }
}
