﻿using System;
using System.Collections.Generic;

namespace Emw.EnergyBids.Contracts
{
    public class Bid
    {
        public Bid() {}

        public Guid Id { get; set; }

        public DateTime CreatedDateUtc { get; set; }

        public DateTime ModifiedDateUtc { get; set; }

        public decimal Price { get; set; }
        
        public Guid? DREventId { get; set; }

        public string MonetizationChannelType { get; set; }

        public string MonetizationChannelId { get; set; }

        public Guid EnergyGroupId { get; set; }

        public BidStatuses Status { get; set; }

        public DateTime BidStartTimeUtc { get; set; }

        public DateTime BidEndTimeUtc { get; set; }

        public List<BidElement> Elements { get; set; }
    }
}
