﻿using Emw.EnergyBids.Contracts;
using Emw.EnergyBids.Storages.TableStorage;
using Ninject.Modules;

namespace Emw.EnergyBids.Configuration
{
    public class NinjectConfigurationModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IBidsStorage>().To<BidsTableStorage>().InTransientScope();
        }
    }
}
