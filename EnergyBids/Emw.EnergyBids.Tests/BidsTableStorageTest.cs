﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Emw.EnergyBids.Contracts;
using Emw.EnergyBids.Storages.TableStorage;
//using Emw.EnergyBids.Storages.TableStorage;
using Microsoft.VisualStudio.TestTools.UnitTesting;

/// <summary>
///     The following differences apply to Table storage in the emulator:
///     Date properties in the Table service in the storage emulator support only the range supported by SQL Server 2005 (they are required to be later than January 1, 1753). All dates before January 1, 1753 are changed to this value.The precision of dates is limited to the precision of SQL Server 2005, meaning that dates are precise to 1/300th of a second.
///     The storage emulator supports partition key and row key property values of less than 512 bytes each. Additionally, the total size of the account name, table name, and key property names together cannot exceed 900 bytes.
///     The total size of a row in a table in the storage emulator is limited to less than 1 MB.
///     In the storage emulator, properties of data type Edm.Guid or Edm.Binary support only the Equal (eq) and NotEqual (ne) comparison operators in query filter strings.
/// </summary>
namespace Emw.EnergyBids.Tests
{
    [TestClass]
    public class BidsTableStorageTest
    {
        private readonly Guid _id = Guid.Parse("10000000-0000-0000-0000-000000000000");
        private readonly Guid _energyGroupId = Guid.Parse("20000000-0000-0000-0000-000000000000");
        private Bid _bid;

        [TestMethod]
        public async Task BidsTableStorageCommonTest()
        {
            await this.CreateBidAsyncTest();
            await this.GetBidByIdAsyncTest();
            await this.GetBidsAsyncTest();
        }
        
        [TestMethod]
        public async Task CreateBidAsyncTest()
        {
            // Arrange
            BidsTableStorage storage = new BidsTableStorage();

            // Act
            await storage.CreateBidAsync(_bid);

            // Assert
            Assert.IsTrue(true);
        }

        [TestMethod]
        public async Task GetBidByIdAsyncTest()
        {
            // Arrange
            BidsTableStorage storage = new BidsTableStorage();

            // Act
            var result = await storage.GetBidByIdAsync(_id);

            // Assert
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task GetBidsAsyncTest()
        {
            // Arrange
            BidsTableStorage storage = new BidsTableStorage();
            
            // Act
            var result1 = await storage.GetBidsAsync(
                    DateTime.UtcNow.AddDays(-1),
                    DateTime.UtcNow.AddDays(1),
                    null);

            // Act
            var result2 = await storage.GetBidsAsync(
                DateTime.UtcNow.AddDays(-1),
                DateTime.UtcNow.AddDays(1),
                _energyGroupId);

            // Assert
            Assert.IsTrue(result1.Any());
            Assert.IsTrue(result2.Any());
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _bid = new Contracts.Bid()
            {
                Id = _id,
                Status = BidStatuses.Proposed,
                EnergyGroupId = _energyGroupId,
                BidEndTimeUtc = DateTime.UtcNow,
                BidStartTimeUtc = DateTime.UtcNow,
                CreatedDateUtc = DateTime.UtcNow,
                DREventId = Guid.NewGuid(),
                ModifiedDateUtc = DateTime.UtcNow,
                MonetizationChannelId = "0",
                MonetizationChannelType = "MonetizationChannelType 0",
                Price = Decimal.MaxValue,
                Elements = new List<BidElement>()
                {
                    new Contracts.BidElement()
                    {
                        StartTimeUtc = DateTime.UtcNow,
                        EndTimeUtc = DateTime.UtcNow,
                        EnergyBidId = Guid.NewGuid(),
                        Id = Guid.NewGuid(),
                        PowerKw = Decimal.MaxValue,
                        EnergyBid = new Contracts.Bid() {Id = Guid.NewGuid(), Status = BidStatuses.Approved,
                            EnergyGroupId = Guid.NewGuid(),
                            BidEndTimeUtc = DateTime.UtcNow,
                            BidStartTimeUtc = DateTime.UtcNow,
                            CreatedDateUtc = DateTime.UtcNow,
                            DREventId = Guid.NewGuid(),
                            ModifiedDateUtc = DateTime.UtcNow,
                            MonetizationChannelId = "0",
                            MonetizationChannelType = "MonetizationChannelType 1",
                            Price = Decimal.MaxValue,
                            Elements = null}
                    },
                    new Contracts.BidElement()
                    {
                        StartTimeUtc = DateTime.UtcNow,
                        EndTimeUtc = DateTime.UtcNow,
                        EnergyBidId = Guid.NewGuid(),
                        Id = Guid.NewGuid(),
                        PowerKw = Decimal.MaxValue,
                        EnergyBid = new Contracts.Bid() {Id = Guid.NewGuid(), Status = BidStatuses.Rejected,
                            EnergyGroupId = Guid.NewGuid(),
                            BidEndTimeUtc = DateTime.UtcNow,
                            BidStartTimeUtc = DateTime.UtcNow,
                            CreatedDateUtc = DateTime.UtcNow,
                            DREventId = Guid.NewGuid(),
                            ModifiedDateUtc = DateTime.UtcNow,
                            MonetizationChannelId = "0",
                            MonetizationChannelType = "MonetizationChannelType 2",
                            Price = Decimal.MaxValue,
                            Elements = null}                    }
                }
            };
        }

        [TestCleanup]
        public async Task TestCleanup()
        {
            BidsTableStorage storage = new BidsTableStorage();
            await storage.RemoveBidByIdAsync(_bid.Id);
        }
    }
}