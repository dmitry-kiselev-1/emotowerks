﻿namespace Emw.EnergyBids.Storages.TableStorage.TableEntity
{
    public enum BidStatuses
    {
        /// <summary>
        /// Bid is created in our system
        /// </summary>
        Proposed,

        /// <summary>
        /// Approved by operator
        /// </summary>
        Approved,

        /// <summary>
        /// Rejected by operator
        /// </summary>
        Rejected,

        /// <summary>
        /// Automatically approved by the system
        /// </summary>
        AutoApproved,

        /// <summary>
        /// Sent to the market, waiting for results
        /// </summary>
        Placed,

        /// <summary>
        /// We've successfully won this bid
        /// </summary>
        Won,

        /// <summary>
        /// We've lost this bid
        /// </summary>
        Lost,
    }
}
