﻿using System;
using System.Collections.Generic;

namespace Emw.EnergyBids.Storages.TableStorage.TableEntity
{
    public class BidElement: Microsoft.WindowsAzure.Storage.Table.TableEntity
    {
        public BidElement() { }

        public Guid Id { get; set; }

        public Guid EnergyBidId { get; set; }

        public EnergyBid EnergyBid { get; set; }

        public DateTime StartTimeUtc { get; set; }

        public DateTime EndTimeUtc { get; set; }

        public decimal PowerKw { get; set; }
    }
}
