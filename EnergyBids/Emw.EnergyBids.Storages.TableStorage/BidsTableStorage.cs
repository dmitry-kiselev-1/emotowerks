﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using Emw.EnergyBids.Contracts;
using AutoMapper;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace Emw.EnergyBids.Storages.TableStorage
{
    public class BidsTableStorage: IBidsStorage
    {
        private static readonly CloudStorageAccount StorageAccount;

        private static readonly string _tableName = "EnergyBid";

        static BidsTableStorage()
        {
            StorageAccount = CloudStorageAccount.Parse(
                ConfigurationManager.ConnectionStrings["BidsTableStorageConnectionString"].ConnectionString);

            InitializeStorage();
            Mapper.Initialize(cfg => cfg.AddProfile<AutoMapperProfile>());
            //Mapper.AssertConfigurationIsValid();
        }

        private static void InitializeStorage()
        {
            CloudTableClient tableClient = StorageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference(_tableName);
            table.CreateIfNotExists();
        }

        public async Task CreateBidAsync(Contracts.Bid contractEntity)
        {
            await this.UpdateBidAsync(contractEntity);
        }

        public async Task UpdateBidAsync(Contracts.Bid contractEntity)
        {
            var tableEntity = Mapper.Map<TableEntity.EnergyBid>(contractEntity);
            tableEntity.RowKey = tableEntity.Id.ToString();
            tableEntity.PartitionKey = tableEntity.Status.ToString();

            TableOperation operation = TableOperation.InsertOrReplace(tableEntity);
            CloudTableClient tableClient = StorageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference(_tableName);

            await table.ExecuteAsync(operation);
        }

        public async Task<Bid> GetBidByIdAsync(Guid id)
        {
            CloudTableClient tableClient = StorageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference(_tableName);

            //TableOperation retrieveOperation = TableOperation.Retrieve<TableEntity.EnergyBid>(id.ToString(), "");

            TableQuery<TableEntity.EnergyBid> query = new TableQuery<TableEntity.EnergyBid>()
                .Where(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, id.ToString()));

            var result = table.ExecuteQuery(query).FirstOrDefault();
            var contractEntity = Mapper.Map<Bid>(result);
            return contractEntity;
        }

        public async Task<IEnumerable<Contracts.Bid>> GetBidsAsync(DateTime startTimeUtc, DateTime endTimeUtc, Guid? energyGroupId)
        {
            CloudTableClient tableClient = StorageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference(_tableName);

            TableQuery<TableEntity.EnergyBid> query = new TableQuery<TableEntity.EnergyBid>()
                .Where(
                    TableQuery.CombineFilters(
                        TableQuery.GenerateFilterConditionForDate("BidStartTimeUtc", QueryComparisons.GreaterThanOrEqual, startTimeUtc),
                        TableOperators.And,
                        TableQuery.GenerateFilterConditionForDate("BidEndTimeUtc", QueryComparisons.LessThanOrEqual, endTimeUtc)
                        )
                );

            var result = energyGroupId.HasValue
                ? table.ExecuteQuery(query).Where(x => x?.EnergyGroupId == energyGroupId.Value)
                : table.ExecuteQuery(query);

            var contractEntity = Mapper.Map<List<Contracts.Bid>>(result.ToList());

            return contractEntity;
        }

        public async Task RemoveBidByIdAsync(Guid id)
        {
            CloudTableClient tableClient = StorageAccount.CreateCloudTableClient();
            CloudTable table = tableClient.GetTableReference(_tableName);

            TableQuery<TableEntity.EnergyBid> query = new TableQuery<TableEntity.EnergyBid>()
                .Where(TableQuery.GenerateFilterCondition("RowKey", QueryComparisons.Equal, id.ToString()));

            var rows = table.ExecuteQuery(query).ToList();

            foreach (var row in rows)
            {
                TableOperation operation = TableOperation.Delete(row);
                await table.ExecuteAsync(operation);
            }
        }
    }
}
