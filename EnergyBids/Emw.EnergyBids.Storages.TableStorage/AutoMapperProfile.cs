﻿using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using AutoMapper;
using Emw.EnergyBids.Contracts;
using Newtonsoft.Json;

namespace Emw.EnergyBids.Storages.TableStorage
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Contracts.Bid, TableEntity.EnergyBid>()
                .ForMember(
                    dest => dest.ElementsCol, 
                    opt => opt.MapFrom(
                        src => JsonConvert.SerializeObject(src.Elements)));

           CreateMap<TableEntity.EnergyBid, Contracts.Bid>()
                .ForMember(dest => dest.Elements, opt => opt.MapFrom(
                   src => JsonConvert.DeserializeObject<IList<Contracts.BidElement>>(src.ElementsCol)));

           CreateMap<Contracts.BidElement, TableEntity.BidElement>().ReverseMap();
        }
    }
}