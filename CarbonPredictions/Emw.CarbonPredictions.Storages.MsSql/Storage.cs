﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Emw.CarbonPredictions.Storages.MsSql.Context;

namespace Emw.CarbonPredictions.Storages.MsSql
{
    public abstract class Storage
    {
        static Storage()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<AutoMapperProfile>();
            });
        }

        protected void SaveChanges(StorageDbContext context)
        {
            bool saveFailed;
            do
            {
                saveFailed = false;
                try
                {
                    context.SaveChanges();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    saveFailed = true;
                    foreach (var entry in ex.Entries)
                    {
                        if (entry.State == EntityState.Deleted)
                        {
                            entry.State = EntityState.Detached;
                        }
                        else
                        {
                            var currentValues = entry.CurrentValues;
                            var databaseValues = entry.GetDatabaseValues();

                            entry.OriginalValues.SetValues(databaseValues);
                            entry.CurrentValues.SetValues(currentValues);
                        }
                    }
                }
                catch (Exception e)
                {
                    //ToDo: NLog
                    Console.WriteLine(e.ToString());
                    //throw;
                }
            } while (saveFailed);
        }
    }
}
