using System.Data.Entity.Migrations;
using Emw.CarbonPredictions.Storages.MsSql.Context;

namespace Emw.CarbonPredictions.Storages.MsSql.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<StorageDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            CommandTimeout = 60 * 60 * 4;
        }

        protected override void Seed(StorageDbContext context)
        {}
    }
}
