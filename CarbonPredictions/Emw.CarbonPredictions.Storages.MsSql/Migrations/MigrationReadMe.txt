﻿
Use package manager console command for migration (after db context changes):

1) set Emw.Markets.Storages.MsSql as start project (prevent connection string error)
2) add-migration "CarbonPredictions"
3) update-database -script
