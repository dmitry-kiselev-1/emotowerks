namespace Emw.CarbonPredictions.Storages.MsSql.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CarbonPredictions",
                c => new
                    {
                        IntervalStartTime = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        ZoneId = c.String(nullable: false, maxLength: 128),
                        MarketId = c.Int(nullable: false),
                        Predicted = c.Decimal(precision: 20, scale: 12),
                        Actual = c.Decimal(precision: 20, scale: 12),
                    })
                .PrimaryKey(t => new { t.IntervalStartTime, t.ZoneId, t.MarketId });
            
            CreateTable(
                "dbo.StageWattTimeV1Actual",
                c => new
                    {
                        created_at = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        ba = c.String(nullable: false, maxLength: 8, unicode: false),
                        carbon = c.Decimal(nullable: false, precision: 20, scale: 12),
                        LogId = c.Long(nullable: false),
                        _year = c.Short(nullable: false),
                        _quarter = c.Byte(nullable: false),
                        _weekOfYear = c.Byte(nullable: false),
                        _month = c.Byte(nullable: false),
                        _day = c.Byte(nullable: false),
                        _dayOfWeek = c.Byte(nullable: false),
                        _hour = c.Byte(nullable: false),
                        _minute = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => new { t.created_at, t.ba })
                .Index(t => t._year)
                .Index(t => t._quarter)
                .Index(t => t._weekOfYear)
                .Index(t => t._month)
                .Index(t => t._day)
                .Index(t => t._dayOfWeek)
                .Index(t => t._hour)
                .Index(t => t._minute);
            
            CreateTable(
                "dbo.StageWattTimeV1ActualLog",
                c => new
                    {
                        LogId = c.Long(nullable: false, identity: true),
                        count = c.Int(nullable: false),
                        StatusCode = c.String(maxLength: 32, unicode: false),
                        ReasonPhrase = c.String(maxLength: 512, unicode: false),
                        start_at = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        end_at = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        ba = c.String(nullable: false, maxLength: 8, unicode: false),
                        page_size = c.Int(nullable: false),
                        freq = c.String(maxLength: 8, unicode: false),
                        market = c.String(maxLength: 8, unicode: false),
                        next = c.String(maxLength: 512, unicode: false),
                    })
                .PrimaryKey(t => t.LogId);
            
            CreateTable(
                "dbo.StageWattTimeV1Predicted",
                c => new
                    {
                        created_at = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        ba = c.String(nullable: false, maxLength: 8, unicode: false),
                        carbon = c.Decimal(nullable: false, precision: 20, scale: 12),
                        LogId = c.Long(nullable: false),
                        _year = c.Short(nullable: false),
                        _quarter = c.Byte(nullable: false),
                        _weekOfYear = c.Byte(nullable: false),
                        _month = c.Byte(nullable: false),
                        _day = c.Byte(nullable: false),
                        _dayOfWeek = c.Byte(nullable: false),
                        _hour = c.Byte(nullable: false),
                        _minute = c.Byte(nullable: false),
                    })
                .PrimaryKey(t => new { t.created_at, t.ba });
            
            CreateTable(
                "dbo.StageWattTimeV1PredictedLog",
                c => new
                    {
                        LogId = c.Long(nullable: false, identity: true),
                        count = c.Int(nullable: false),
                        StatusCode = c.String(maxLength: 32, unicode: false),
                        ReasonPhrase = c.String(maxLength: 512, unicode: false),
                        start_at = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        end_at = c.DateTime(nullable: false, precision: 0, storeType: "datetime2"),
                        ba = c.String(nullable: false, maxLength: 8, unicode: false),
                        page_size = c.Int(nullable: false),
                        freq = c.String(maxLength: 8, unicode: false),
                        market = c.String(maxLength: 8, unicode: false),
                        next = c.String(maxLength: 512, unicode: false),
                    })
                .PrimaryKey(t => t.LogId);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.StageWattTimeV1Actual", new[] { "_minute" });
            DropIndex("dbo.StageWattTimeV1Actual", new[] { "_hour" });
            DropIndex("dbo.StageWattTimeV1Actual", new[] { "_dayOfWeek" });
            DropIndex("dbo.StageWattTimeV1Actual", new[] { "_day" });
            DropIndex("dbo.StageWattTimeV1Actual", new[] { "_month" });
            DropIndex("dbo.StageWattTimeV1Actual", new[] { "_weekOfYear" });
            DropIndex("dbo.StageWattTimeV1Actual", new[] { "_quarter" });
            DropIndex("dbo.StageWattTimeV1Actual", new[] { "_year" });
            DropTable("dbo.StageWattTimeV1PredictedLog");
            DropTable("dbo.StageWattTimeV1Predicted");
            DropTable("dbo.StageWattTimeV1ActualLog");
            DropTable("dbo.StageWattTimeV1Actual");
            DropTable("dbo.CarbonPredictions");
        }
    }
}
