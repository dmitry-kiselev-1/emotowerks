﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AutoMapper;
using Emw.Common;
using Emw.CarbonPredictions.Contracts;
using Emw.CarbonPredictions.Storages.MsSql.Context;

namespace Emw.CarbonPredictions.Storages.MsSql
{
    public class CarbonPredictionsStorage : Storage, ICarbonPredictionsStorage
    {
        private const string DefaultSubLapId = "SLAP_PGCC-APND";
        private const int DefaultMarketId = 3; // ('RTM', 'Real-time market','CaIso')

        public async Task<List<Contracts.CarbonPrediction>> GetPredictedIntervalsAsync(string subLapId, DateTimeRange interval)
        {
            List<Contracts.CarbonPrediction> result = new List<Contracts.CarbonPrediction>();

            bool martIsEmpty = await this.IsEmpty();

            if (!martIsEmpty)
            {
                using (var context = new StorageDbContext() { Configuration = { AutoDetectChangesEnabled = false, ValidateOnSaveEnabled = false } })
                {
                    var lastPredictedCarbon = context.PredictedCarbon.AsNoTracking()
                        .Where(p =>
                            (p.ZoneId == DefaultSubLapId) &&
                            (p.IntervalStartTime >= interval.From) &&
                            (p.IntervalStartTime <= interval.To))
                        .OrderByDescending(p => p.IntervalStartTime)
                        .ToList();

                    return Mapper.Map<List<Contracts.CarbonPrediction>>(lastPredictedCarbon);
                }
            }
            else
            {
                //return null;
                
                // ToDo: remove stub (random data):

                var random = new Random();

                foreach (var i in interval.Slice(new TimeSpan(hours: 0, minutes: 5, seconds: 0)))
                {
                    int lower = 835; // 835.689004328325
                    int upper = 1432; // 1432.251964781660

                    var valueIntegerPart = (decimal) (random.Next(lower, upper)); 
                    var valueFractionalPart1 = (decimal) (random.Next(1, 1000000) / (decimal) 1000000);
                    var valueFractionalPart2 = (decimal) (random.Next(1, 1000000) / (decimal) 1000000000000);

                    var value = (valueIntegerPart + valueFractionalPart1 + valueFractionalPart2);
                    //var value = (decimal)(random.Next(lower, upper) + (random.Next(1, 1000000000) / (decimal)100000000)); // 1249.900269616057

                    bool isPredicted = random.Next(0, 2) == 1;

                    result.Add(new Contracts.CarbonPrediction()
                    {
                        IntervalStartTime = i.From,
                        ZoneId = DefaultSubLapId,
                        MarketId = DefaultMarketId,
                        Actual = isPredicted ? (decimal?) null : value,
                        Predicted = isPredicted ? value : (decimal?) null
                    });
                }
            }

            return await Task.Run(() => result);
        }

        public async Task<List<Contracts.CarbonPrediction>> GetPredictedIntervalsAsync(string subLapId, DateTime from, DateTime to)
        {
            using (var context = new StorageDbContext() { Configuration = { AutoDetectChangesEnabled = false, ValidateOnSaveEnabled = false } })
            {
                var lastPredictedCarbon = await context.PredictedCarbon.AsNoTracking()
                    .Where(p =>
                        (p.ZoneId == DefaultSubLapId) &&
                        (p.IntervalStartTime >= from) &&
                        (p.IntervalStartTime <= to))
                    .ToListAsync();

                return Mapper.Map<List<Contracts.CarbonPrediction>>(lastPredictedCarbon);
            }
        }

        public async Task<bool> IsEmpty(string subLapId = DefaultSubLapId)
        {
            using (var context = new StorageDbContext() { Configuration = { AutoDetectChangesEnabled = false, ValidateOnSaveEnabled = false } })
            {
                return !await context.PredictedCarbon
                    .AsNoTracking()
                    .AnyAsync(p => (p.ZoneId == DefaultSubLapId) && (p.MarketId == DefaultMarketId));
            }
        }

        public async Task<Contracts.CarbonPrediction> GetLastAsync(string subLapId = DefaultSubLapId)
        {
            using (var context = new StorageDbContext() { Configuration = { AutoDetectChangesEnabled = false, ValidateOnSaveEnabled = false } })
            {
                var lastPredictedCarbon = await context.PredictedCarbon
                .AsNoTracking()
                .OrderByDescending(p => p.IntervalStartTime)
                .FirstOrDefaultAsync(p => (p.ZoneId == DefaultSubLapId) && (p.MarketId == DefaultMarketId));

                return Mapper.Map<Contracts.CarbonPrediction>(lastPredictedCarbon);
            }
        }

        public async Task<Contracts.CarbonPrediction> GetFirstAsync(string subLapId = DefaultSubLapId)
        {
            using (var context = new StorageDbContext() { Configuration = { AutoDetectChangesEnabled = false, ValidateOnSaveEnabled = false } })
            {
                var lastPredictedCarbon = await context.PredictedCarbon
                    .AsNoTracking()
                    .OrderBy(p => p.IntervalStartTime)
                    .FirstOrDefaultAsync(p => (p.ZoneId == DefaultSubLapId) && (p.MarketId == DefaultMarketId));

                return Mapper.Map<Contracts.CarbonPrediction>(lastPredictedCarbon);
            }
        }

        public async Task<Contracts.CarbonPrediction> GetFirstEmptyActualAsync(string subLapId = DefaultSubLapId)
        {
            using (var context = new StorageDbContext() { Configuration = { AutoDetectChangesEnabled = false, ValidateOnSaveEnabled = false } })
            {
                var lastPredictedCarbon = await context.PredictedCarbon
                    .AsNoTracking()
                    .OrderBy(p => p.IntervalStartTime)
                    .FirstOrDefaultAsync(p => (p.ZoneId == DefaultSubLapId) && (p.MarketId == DefaultMarketId) && (p.Actual == null));

                return Mapper.Map<Contracts.CarbonPrediction>(lastPredictedCarbon);
            }
        }

        public async Task<bool> AddPointsInterval(List<Contracts.CarbonPrediction> pointsInterval)
        {
            if (pointsInterval == null || !pointsInterval.Any()) return true;

            var mapped = Mapper.Map<List<Context.CarbonPrediction>>(pointsInterval);

            foreach (var m in mapped)
            {
                m.MarketId = DefaultMarketId;
                m.ZoneId = DefaultSubLapId;
            }

            using (var context = new StorageDbContext() { Configuration = {AutoDetectChangesEnabled = false, ValidateOnSaveEnabled = false }})
            {
                context.PredictedCarbon.AddRange(mapped);
                this.SaveChanges(context);
            }

            return await Task.Run(() => true);
        }

        public async Task<int> RemovePointsInterval(DateTime from)
        {
            using (var context = new StorageDbContext() { Configuration = { AutoDetectChangesEnabled = false, ValidateOnSaveEnabled = false } })
            {
                return await context.Database.ExecuteSqlCommandAsync("DELETE CarbonPredictions WHERE IntervalStartTime >= @from;", new SqlParameter("@from", from));
            }
        }

        public async Task<bool> UpdateActual(List<Contracts.CarbonPrediction> pointsInterval)
        {
            if (pointsInterval == null || !pointsInterval.Any()) return true;

            using (var context = new StorageDbContext() { Configuration = { AutoDetectChangesEnabled = true, ValidateOnSaveEnabled = false } })
            {
                int n = 1;
                foreach (var point in pointsInterval)
                {
                    var predictedCarbon = context.PredictedCarbon?.Find(point.IntervalStartTime, DefaultSubLapId, DefaultMarketId);

                    if (predictedCarbon != null)
                    {
                        predictedCarbon.Actual = point.Actual;
                    }
                    Console.WriteLine($"Update {n++} of {pointsInterval.Count}    {predictedCarbon?.IntervalStartTime}    P: {predictedCarbon?.Predicted} A: {predictedCarbon?.Actual}");
                }
                this.SaveChanges(context);
            }
            return await Task.Run(() => true);
        }
    }
}