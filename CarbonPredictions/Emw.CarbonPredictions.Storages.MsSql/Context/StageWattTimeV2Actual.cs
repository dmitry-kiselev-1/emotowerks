using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Emw.CarbonPredictions.Storages.MsSql.Context
{
    public partial class StageWattTimeV2Actual
    {
        /// <summary> ISO8601 format date/time for when a new carbon index signal will be available. Defaults to UTC if Timezone is not specified. 2018-04-11T20:52:30.187Z </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public System.DateTime validUntil { get; set; }

        /// <summary> A positive integer between 0 and 100 that represents how clean 0 or dirty (100) the grid is at the moment. </summary>
        [Required]
        public int percent { get; set; }

        [Required]
        public int rating { get; set; }


        /// <summary> A binary integer between 0 and 1 that represents if the grid is clean (1) or dirty (0) to consume electricity. </summary>
        [Required]
        public int Switch { get; set; }

        /// <summary> abbreviation of Balancing Authority </summary>
        [Required]
        public string ba { get; set; }
    }
}
