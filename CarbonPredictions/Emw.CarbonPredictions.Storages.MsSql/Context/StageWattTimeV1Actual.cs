using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Emw.CarbonPredictions.Storages.MsSql.Context
{
    public partial class StageWattTimeV1Actual
    {
        public System.DateTime created_at { get; set; }
        public string ba { get; set; }
        public decimal carbon { get; set; }
        public long LogId { get; set; }
        public Int16 _year { get; set; }
        public byte _quarter { get; set; }
        public byte _weekOfYear { get; set; }
        public byte _month { get; set; }
        public byte _day { get; set; }
        public byte _dayOfWeek { get; set; }
        public byte _hour { get; set; }
        public byte _minute { get; set; }
    }
}
