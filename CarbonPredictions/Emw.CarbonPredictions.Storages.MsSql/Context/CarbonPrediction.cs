﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;

namespace Emw.CarbonPredictions.Storages.MsSql.Context
{
    [DebuggerDisplay("[{" + nameof(ZoneId) + "}, {" + nameof(IntervalStartTime) + "}, {" + nameof(Predicted) + "}]")]
    public class CarbonPrediction
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public DateTime IntervalStartTime { get; set; }

        //[Required]
        //[DataType(DataType.Date)]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        //public DateTime PredictionDate { get; set; }

        [Key]
        [Required]
        public int MarketId { get; set; } = 3; // ('RTM', 'Real-time market','CaIso')

        [Key]
        [Required]
        public string ZoneId { get; set; } = "SLAP_PGCC-APND"; // first CAISO slap
        //public virtual Zone Zone { get; set; }

        public decimal? Predicted { get; set; }

        public decimal? Actual { get; set; }
    }
}
