﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Emw.CarbonPredictions.Storages.MsSql.Context.Convertions;

namespace Emw.CarbonPredictions.Storages.MsSql.Context
{
    public class StorageDbContext : DbContext
    {
        public virtual DbSet<StageWattTimeV1Actual> StageWattTimeV1Actual { get; set; }
        public virtual DbSet<StageWattTimeV1ActualLog> StageWattTimeV1ActualLog { get; set; }
        public virtual DbSet<StageWattTimeV1Predicted> StageWattTimeV1Predicted { get; set; }
        public virtual DbSet<StageWattTimeV1PredictedLog> StageWattTimeV1PredictedLog { get; set; }
        public virtual DbSet<CarbonPrediction> PredictedCarbon { get; set; }

        public StorageDbContext() : base("name=CarbonPredictionsConnectionString")
        {
            var x = typeof(System.Data.Entity.SqlServer.SqlProviderServices);
        }

        public StorageDbContext(string connection) : base(connection)
        {
            var x = typeof(System.Data.Entity.SqlServer.SqlProviderServices);
        }

        public static StorageDbContext CreateWithConnectionString()
        {
            return new StorageDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Add(new DataTypePropertyAttributeConvention());

            modelBuilder.Entity<StageWattTimeV1Actual>().HasKey(k => new { k.created_at, k.ba });
            modelBuilder.Entity<StageWattTimeV1Actual>().Property(x => x.created_at).HasColumnType("datetime2").HasPrecision(0);
            modelBuilder.Entity<StageWattTimeV1Actual>().Property(x => x.ba).HasMaxLength(8).IsUnicode(false);
            modelBuilder.Entity<StageWattTimeV1Actual>().Property(x => x.carbon).HasPrecision(20, 12).IsRequired();
            modelBuilder.Entity<StageWattTimeV1Actual>().Property(x => x._year).HasColumnType("smallint").IsRequired();
            modelBuilder.Entity<StageWattTimeV1Actual>().Property(x => x._quarter).HasColumnType("tinyint").IsRequired();
            modelBuilder.Entity<StageWattTimeV1Actual>().Property(x => x._weekOfYear).HasColumnType("tinyint").IsRequired();
            modelBuilder.Entity<StageWattTimeV1Actual>().Property(x => x._month).HasColumnType("tinyint").IsRequired();
            modelBuilder.Entity<StageWattTimeV1Actual>().Property(x => x._day).HasColumnType("tinyint").IsRequired();
            modelBuilder.Entity<StageWattTimeV1Actual>().Property(x => x._dayOfWeek).HasColumnType("tinyint").IsRequired();
            modelBuilder.Entity<StageWattTimeV1Actual>().Property(x => x._hour).HasColumnType("tinyint").IsRequired();
            modelBuilder.Entity<StageWattTimeV1Actual>().Property(x => x._minute).HasColumnType("tinyint").IsRequired();
            modelBuilder.Entity<StageWattTimeV1Actual>().HasIndex(x => x._year);
            modelBuilder.Entity<StageWattTimeV1Actual>().HasIndex(x => x._quarter);
            modelBuilder.Entity<StageWattTimeV1Actual>().HasIndex(x => x._weekOfYear);
            modelBuilder.Entity<StageWattTimeV1Actual>().HasIndex(x => x._month);
            modelBuilder.Entity<StageWattTimeV1Actual>().HasIndex(x => x._day);
            modelBuilder.Entity<StageWattTimeV1Actual>().HasIndex(x => x._dayOfWeek);
            modelBuilder.Entity<StageWattTimeV1Actual>().HasIndex(x => x._hour);
            modelBuilder.Entity<StageWattTimeV1Actual>().HasIndex(x => x._minute);
            modelBuilder.Entity<StageWattTimeV1ActualLog>().HasKey(k => k.LogId);
            modelBuilder.Entity<StageWattTimeV1ActualLog>().Property(x => x.LogId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<StageWattTimeV1ActualLog>().Property(x => x.StatusCode).HasMaxLength(32).IsUnicode(false);
            modelBuilder.Entity<StageWattTimeV1ActualLog>().Property(x => x.ReasonPhrase).HasMaxLength(512).IsUnicode(false);
            modelBuilder.Entity<StageWattTimeV1ActualLog>().Property(x => x.start_at).HasColumnType("datetime2").HasPrecision(0).IsRequired();
            modelBuilder.Entity<StageWattTimeV1ActualLog>().Property(x => x.end_at).HasColumnType("datetime2").HasPrecision(0).IsRequired();
            modelBuilder.Entity<StageWattTimeV1ActualLog>().Property(x => x.ba).HasMaxLength(8).IsUnicode(false).IsRequired();
            modelBuilder.Entity<StageWattTimeV1ActualLog>().Property(x => x.freq).HasMaxLength(8).IsUnicode(false);
            modelBuilder.Entity<StageWattTimeV1ActualLog>().Property(x => x.market).HasMaxLength(8).IsUnicode(false);
            modelBuilder.Entity<StageWattTimeV1ActualLog>().Property(x => x.next).HasMaxLength(512).IsUnicode(false);

            modelBuilder.Entity<StageWattTimeV1Predicted>().HasKey(k => new { k.created_at, k.ba });
            modelBuilder.Entity<StageWattTimeV1Predicted>().Property(x => x.created_at).HasColumnType("datetime2").HasPrecision(0);
            modelBuilder.Entity<StageWattTimeV1Predicted>().Property(x => x.ba).HasMaxLength(8).IsUnicode(false);
            modelBuilder.Entity<StageWattTimeV1Predicted>().Property(x => x.carbon).HasPrecision(20, 12).IsRequired();
            modelBuilder.Entity<StageWattTimeV1Predicted>().Property(x => x._year).HasColumnType("smallint").IsRequired();
            modelBuilder.Entity<StageWattTimeV1Predicted>().Property(x => x._quarter).HasColumnType("tinyint").IsRequired();
            modelBuilder.Entity<StageWattTimeV1Predicted>().Property(x => x._weekOfYear).HasColumnType("tinyint").IsRequired();
            modelBuilder.Entity<StageWattTimeV1Predicted>().Property(x => x._month).HasColumnType("tinyint").IsRequired();
            modelBuilder.Entity<StageWattTimeV1Predicted>().Property(x => x._day).HasColumnType("tinyint").IsRequired();
            modelBuilder.Entity<StageWattTimeV1Predicted>().Property(x => x._dayOfWeek).HasColumnType("tinyint").IsRequired();
            modelBuilder.Entity<StageWattTimeV1Predicted>().Property(x => x._hour).HasColumnType("tinyint").IsRequired();
            modelBuilder.Entity<StageWattTimeV1Predicted>().Property(x => x._minute).HasColumnType("tinyint").IsRequired();
            //modelBuilder.Entity<StageWattTimeV1Predicted>().HasIndex(x => x._year);
            //modelBuilder.Entity<StageWattTimeV1Predicted>().HasIndex(x => x._quarter);
            //modelBuilder.Entity<StageWattTimeV1Predicted>().HasIndex(x => x._weekOfYear);
            //modelBuilder.Entity<StageWattTimeV1Predicted>().HasIndex(x => x._month);
            //modelBuilder.Entity<StageWattTimeV1Predicted>().HasIndex(x => x._day);
            //modelBuilder.Entity<StageWattTimeV1Predicted>().HasIndex(x => x._dayOfWeek);
            //modelBuilder.Entity<StageWattTimeV1Predicted>().HasIndex(x => x._hour);
            //modelBuilder.Entity<StageWattTimeV1Predicted>().HasIndex(x => x._minute);
            modelBuilder.Entity<StageWattTimeV1PredictedLog>().HasKey(k => k.LogId);
            modelBuilder.Entity<StageWattTimeV1PredictedLog>().Property(x => x.LogId).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            modelBuilder.Entity<StageWattTimeV1PredictedLog>().Property(x => x.StatusCode).HasMaxLength(32).IsUnicode(false);
            modelBuilder.Entity<StageWattTimeV1PredictedLog>().Property(x => x.ReasonPhrase).HasMaxLength(512).IsUnicode(false);
            modelBuilder.Entity<StageWattTimeV1PredictedLog>().Property(x => x.start_at).HasColumnType("datetime2").HasPrecision(0).IsRequired();
            modelBuilder.Entity<StageWattTimeV1PredictedLog>().Property(x => x.end_at).HasColumnType("datetime2").HasPrecision(0).IsRequired();
            modelBuilder.Entity<StageWattTimeV1PredictedLog>().Property(x => x.ba).HasMaxLength(8).IsUnicode(false).IsRequired();
            modelBuilder.Entity<StageWattTimeV1PredictedLog>().Property(x => x.freq).HasMaxLength(8).IsUnicode(false);
            modelBuilder.Entity<StageWattTimeV1PredictedLog>().Property(x => x.market).HasMaxLength(8).IsUnicode(false);
            modelBuilder.Entity<StageWattTimeV1PredictedLog>().Property(x => x.next).HasMaxLength(512).IsUnicode(false);

            // Entity Framework TPC:
            modelBuilder.Entity<StageWattTimeV1Predicted>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("StageWattTimeV1Predicted");
            });
            
            // Entity Framework TPC:
            modelBuilder.Entity<StageWattTimeV1PredictedLog>().Map(m =>
            {
                m.MapInheritedProperties();
                m.ToTable("StageWattTimeV1PredictedLog");
            });

            modelBuilder.Entity<CarbonPrediction>()
                .HasKey(k => new { k.IntervalStartTime, k.ZoneId, k.MarketId });
            //  .HasKey(k => new { k.ZoneId, k.IntervalStartTime, k.PredictionDate, k.MarketId });
            modelBuilder.Entity<CarbonPrediction>().Property(x => x.Predicted).HasPrecision(20, 12);
            modelBuilder.Entity<CarbonPrediction>().Property(x => x.Actual).HasPrecision(20, 12);
            modelBuilder.Entity<CarbonPrediction>().Property(x => x.IntervalStartTime).HasColumnType("datetime2").HasPrecision(0);
        }
    }
}
