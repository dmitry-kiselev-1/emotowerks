﻿using AutoMapper;

namespace Emw.CarbonPredictions.Storages.MsSql
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<
                Context.StageWattTimeV1ActualLog,
                Contracts.WattTimeV1.StageWattTimeV1ActualLog>().ReverseMap(); ;

            CreateMap<
                Context.StageWattTimeV1Actual,
                Contracts.WattTimeV1.StageWattTimeV1Actual>().ReverseMap(); ;

            CreateMap<
                Context.CarbonPrediction,
                Contracts.CarbonPrediction>().ReverseMap(); ;
        }
    }
}