﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServer;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Emw.CarbonPredictions.Storages.MsSql.Context;
using Emw.CarbonPredictions.Contracts.WattTimeV1;
using AutoMapper;
using AutoMapper.QueryableExtensions;

namespace Emw.CarbonPredictions.Storages.MsSql
{
    public class WattTimeV1aStadeStorage : Storage, IWattTimeV1aStadeStorage
    {
        public async Task<bool> IsEmpty(string ba)
        {
            using (var context = new StorageDbContext() { Configuration = { AutoDetectChangesEnabled = false, ValidateOnSaveEnabled = false } })
            {
                return !await context.StageWattTimeV1Actual.AsNoTracking().AnyAsync();
            }
        }

        public async Task<Contracts.WattTimeV1.StageWattTimeV1ActualLog> GetLastLog()
        {
            using (var context = new StorageDbContext() { Configuration = { AutoDetectChangesEnabled = false, ValidateOnSaveEnabled = false } })
            {
                var maxLogId = await context.StageWattTimeV1ActualLog.AnyAsync() ? await context.StageWattTimeV1ActualLog.MaxAsync(x => x.LogId) : 0;
                var maxLog = maxLogId != 0 ? await context.StageWattTimeV1ActualLog.FindAsync(maxLogId) : null;
                return Mapper.Map<Contracts.WattTimeV1.StageWattTimeV1ActualLog>(maxLog);
            }
        }

        public async Task<Contracts.WattTimeV1.StageWattTimeV1ActualLog> GetLog(long id)
        {
            using (var context = new StorageDbContext() { Configuration = { AutoDetectChangesEnabled = false, ValidateOnSaveEnabled = false } })
            {
                var log = await context.StageWattTimeV1ActualLog.FindAsync(id);
                return Mapper.Map<Contracts.WattTimeV1.StageWattTimeV1ActualLog>(log);
            }
        }

        public async Task<Contracts.WattTimeV1.StageWattTimeV1ActualLog> GetLog(DateTime startAt, DateTime endAt)
        {
            using (var context = new StorageDbContext() { Configuration = { AutoDetectChangesEnabled = false, ValidateOnSaveEnabled = false } })
            {
                var log = await context.StageWattTimeV1ActualLog.FirstOrDefaultAsync(x =>
                    (x.start_at == startAt) && (x.end_at == endAt));

                return Mapper.Map<Contracts.WattTimeV1.StageWattTimeV1ActualLog>(log);
            }
        }

        public async Task<long> AddLog(Contracts.WattTimeV1.StageWattTimeV1ActualLog log)
        {
            using (var context = new StorageDbContext() { Configuration = { AutoDetectChangesEnabled = true, ValidateOnSaveEnabled = false } })
            {
                var mapped = Mapper.Map<Context.StageWattTimeV1ActualLog>(log);
                var inserted = context.StageWattTimeV1ActualLog.Add(mapped);
                this.SaveChanges(context);
                return await Task.Run(() => inserted.LogId);
            }
        }

        public async Task<long> AddPoint(Contracts.WattTimeV1.StageWattTimeV1Actual point)
        {
            using (var context = new StorageDbContext() { Configuration = { AutoDetectChangesEnabled = false, ValidateOnSaveEnabled = false } })
            {
                var mapped = Mapper.Map<Context.StageWattTimeV1Actual>(point);
                var inserted = context.StageWattTimeV1Actual.Add(mapped);
                this.SaveChanges(context);
                return await Task.Run(() => inserted.LogId);
            }
        }

        public async Task<int> DeleteLog(long logId)
        {
            using (var context = new StorageDbContext() { Configuration = { AutoDetectChangesEnabled = false, ValidateOnSaveEnabled = false } })
            {
                await context.Database.ExecuteSqlCommandAsync("DELETE StageWattTimeV1ActualLog WHERE LogId = @LogId;", new SqlParameter("@logId", logId));
                await context.Database.ExecuteSqlCommandAsync("DELETE StageWattTimeV1Actual WHERE LogId = @logId;", new SqlParameter("@logId", logId));
                return 0;
            }
        }

        public async Task<int> Clear()
        {
            using (var context = new StorageDbContext() { Configuration = { AutoDetectChangesEnabled = false, ValidateOnSaveEnabled = false } })
            {
                await context.Database.ExecuteSqlCommandAsync("DELETE dbo.StageWattTimeV1ActualLod;");
                await context.Database.ExecuteSqlCommandAsync("DELETE dbo.StageWattTimeV1Actual;");
                return 0;
            }
        }

        public async Task<List<Contracts.WattTimeV1.StageWattTimeV1Actual>> GetPointsByDayOfWeek(string ba, DateTime from, DateTime to)
        {
            using (var context = new StorageDbContext() { Configuration = { AutoDetectChangesEnabled = false, ValidateOnSaveEnabled = false } })
            {
                if (await this.IsEmpty(ba)) return null;
                if (from >= to) return null;

                int intervalLenInMin = to.Minute - from.Minute;

                DateTime currentFrom = from;
                DateTime currentTo = to;

                List<Context.StageWattTimeV1Actual> points = new List<Context.StageWattTimeV1Actual>();

                var firstPoint = await context.StageWattTimeV1Actual.AsNoTracking().OrderBy(p => p.created_at).FirstOrDefaultAsync();

                while (!points.Any() && (currentFrom >= firstPoint?.created_at))
                {
                    points = context.StageWattTimeV1Actual
                        .AsNoTracking()
                        .Where(p => (p.ba == ba) &&
                                    (p._dayOfWeek == (byte)currentFrom.DayOfWeek) &&
                                    (p._hour == (byte)currentFrom.Hour) &&
                                    (p._minute >= currentFrom.Minute) && (p._minute < currentTo.Minute)
                        ).ToList();

                    // it's mean there are no changes in source data on current interval, so we have to find last changes:
                    if (!points.Any())
                    {
                        currentFrom = currentFrom.AddMinutes(-intervalLenInMin);
                        currentTo = currentTo.AddMinutes(-intervalLenInMin);
                    }
                }

                var mapped = Mapper.Map<List<Contracts.WattTimeV1.StageWattTimeV1Actual>>(points);
                return await Task.Run(() => mapped);
            }
        }

        public async Task<Contracts.WattTimeV1.StageWattTimeV1Actual> GetPoint(string ba, DateTime from, DateTime to)
        {
            using (var context = new StorageDbContext() { Configuration = { AutoDetectChangesEnabled = false, ValidateOnSaveEnabled = false } })
            {
                if (await this.IsEmpty(ba)) return null;
                if (from >= to) return null;

                int intervalLenInMin = to.Minute - from.Minute;

                DateTime currentFrom = from;
                DateTime currentTo = to;

                Context.StageWattTimeV1Actual point = null;

                var firstPoint = await context.StageWattTimeV1Actual.AsNoTracking().OrderBy(p => p.created_at).FirstOrDefaultAsync();
                
                // find actual points only inside hour:
                while ((point == null) && (currentFrom.Hour == from.Hour) && (currentFrom >= firstPoint?.created_at))
                {
                    point = await context.StageWattTimeV1Actual
                        .AsNoTracking()
                        //.OrderBy(p => p.created_at)
                        .FirstOrDefaultAsync(p => (p.ba == ba) &&
                                             (p._year == (short)currentFrom.Year) &&
                                             (p._month == (byte)currentFrom.Month) &&
                                             (p._day == (byte)currentFrom.Day) &&
                                             (p._hour == (byte)currentFrom.Hour) &&
                                             (p.created_at <= to)
                    );

                    // it's mean there are no changes in source data on current interval, so we have to find last changes:
                    if (point == null)
                    {
                        currentFrom = currentFrom.AddMinutes(-intervalLenInMin);
                        currentTo = currentTo.AddMinutes(-intervalLenInMin);
                    }
                }

                var mapped = Mapper.Map<Contracts.WattTimeV1.StageWattTimeV1Actual>(point);
                return await Task.Run(() => mapped);
            }
        }
    }
}
