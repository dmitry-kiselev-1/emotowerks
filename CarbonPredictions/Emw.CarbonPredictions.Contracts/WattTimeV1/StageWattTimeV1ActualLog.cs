namespace Emw.CarbonPredictions.Contracts.WattTimeV1
{
    public partial class StageWattTimeV1ActualLog
    {
        public long LogId { get; set; }
        public int count { get; set; }
        public string StatusCode { get; set; }
        public string ReasonPhrase { get; set; }
        public System.DateTime start_at { get; set; }
        public System.DateTime end_at { get; set; }
        public string ba { get; set; }
        public int page_size { get; set; }
        public string freq { get; set; }
        public string market { get; set; }
        public string next { get; set; }
    }
}
