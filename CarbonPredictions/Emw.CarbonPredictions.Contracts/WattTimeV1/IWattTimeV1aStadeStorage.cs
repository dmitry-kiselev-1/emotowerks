﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Emw.CarbonPredictions.Contracts.WattTimeV1
{
    public interface IWattTimeV1aStadeStorage
    {
        Task<bool> IsEmpty(string ba);

        Task<Contracts.WattTimeV1.StageWattTimeV1ActualLog> GetLastLog();

        Task<Contracts.WattTimeV1.StageWattTimeV1ActualLog> GetLog(long id);

        Task<Contracts.WattTimeV1.StageWattTimeV1ActualLog> GetLog(DateTime startAt, DateTime endAt);
        
        Task<long> AddLog(Contracts.WattTimeV1.StageWattTimeV1ActualLog log);

        Task<long> AddPoint(Contracts.WattTimeV1.StageWattTimeV1Actual point);

        Task<int> DeleteLog(long logId);

        Task<int> Clear();

        Task<List<Contracts.WattTimeV1.StageWattTimeV1Actual>> GetPointsByDayOfWeek(string ba, DateTime from, DateTime to);

        Task<Contracts.WattTimeV1.StageWattTimeV1Actual> GetPoint(string ba, DateTime from, DateTime to);
    }
}
