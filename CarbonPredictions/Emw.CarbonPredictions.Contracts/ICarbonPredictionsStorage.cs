﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Emw.Common;

namespace Emw.CarbonPredictions.Contracts
{
    public interface ICarbonPredictionsStorage
    {
        Task<List<CarbonPrediction>> GetPredictedIntervalsAsync(string subLapId, DateTimeRange interval);

        Task<List<CarbonPrediction>> GetPredictedIntervalsAsync(string subLapId, DateTime from, DateTime to);

        Task<bool> IsEmpty(string subLapId = "");

        Task<CarbonPrediction> GetLastAsync(string subLapId = "");

        Task<CarbonPrediction> GetFirstAsync(string subLapId = "");

        Task<CarbonPrediction> GetFirstEmptyActualAsync(string subLapId = "");

        Task<bool> AddPointsInterval(List<Contracts.CarbonPrediction> pointsInterval);

        Task<int> RemovePointsInterval(DateTime from);

        Task<bool> UpdateActual(List<Contracts.CarbonPrediction> pointsInterval);
    }
}
