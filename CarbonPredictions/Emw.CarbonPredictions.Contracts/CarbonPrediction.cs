﻿using System;
using System.Diagnostics;

namespace Emw.CarbonPredictions.Contracts
{
    [DebuggerDisplay("[{" + nameof(ZoneId) + "}, {" + nameof(IntervalStartTime) + "}, {" + nameof(Predicted) + "}]")]
    public class CarbonPrediction
    {
        public DateTime IntervalStartTime { get; set; }

        public int MarketId { get; set; }

        public string ZoneId { get; set; }

        public decimal? Predicted { get; set; }

        public decimal? Actual { get; set; }
    }
}
