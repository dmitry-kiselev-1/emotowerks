﻿using System;
using System.Threading.Tasks;
using Emw.CarbonPredictions.ETL.Stage;

namespace Emw.WebJobs.WattTimeV1aStadeETL
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () =>
                {
                    Console.WriteLine("Start Emw.WebJobs.WattTimeV1aStadeETL");
                    Console.WriteLine();

                    var code = await new WattTimeV1a().LoadAsync();

                    Console.WriteLine();
                    Console.WriteLine($"Stop Emw.WebJobs.WattTimeV1aStadeETL code = {code}");

                    //Console.ReadLine();
                })
                .GetAwaiter().GetResult();
        }
    }
}
