﻿using System;
using System.Threading.Tasks;
using Emw.CarbonPredictions.ETL.Mart;

namespace Emw.WebJobs.WattTimeV1aMartETL
{
    class Program
    {
        static void Main()
        {
            Task.Run(async () =>
                {

                    Console.WriteLine("Start Emw.WebJobs.WattTimeV1aMartETL");
                    Console.WriteLine();

                    var code = await new WattTimeV1aPredictions().LoadAsync();

                    Console.WriteLine();
                    Console.WriteLine($"Stop Emw.WebJobs.WattTimeV1aMartETL code = {code}");

                    //Console.ReadLine();
                })
                .GetAwaiter().GetResult();
        }
    }
}
