﻿using Emw.CarbonPredictions.Contracts;
using Emw.CarbonPredictions.Contracts.WattTimeV1;
using Emw.CarbonPredictions.Storages.MsSql;
using Ninject.Modules;

namespace Emw.CarbonPredictions.Configuration
{
    public class NinjectConfigurationModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ICarbonPredictionsStorage>().To<CarbonPredictionsStorage>().InTransientScope();
            Bind<IWattTimeV1aStadeStorage>().To<WattTimeV1aStadeStorage>().InTransientScope();
        }
    }
}