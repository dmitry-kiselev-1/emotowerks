﻿using System;
using System.Collections.Specialized;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using Emw.CarbonPredictions.Configuration;
using Emw.CarbonPredictions.Contracts;
using Emw.CarbonPredictions.Contracts.WattTimeV1;
using Newtonsoft.Json.Linq;

namespace Emw.CarbonPredictions.ETL.Stage
{
    public abstract class WattTimeV1: Etl
    {
        private static readonly HttpClient HttpClient = new HttpClient();

        static WattTimeV1()
        {
            //System.Net.ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, errors) => true;
        }

        /// <summary>
        /// Load WattTime Version 1 api actual CO2 data to stage storage
        /// </summary>
        /// <param name="ba">An abbreviation for a balancing authority. Options can be found at the 'balancing_authorities' endpoint. e.g., ba=ISONE</param>
        /// <param name="pageSize">Number of data points to return on each page. default is page_size=100, max is page_size=1000.</param>
        /// <param name="freq">Time series frequency. Options are '5m', '10m', '1hr', 'n/a'. e.g., freq=1hr</param>
        /// <param name="market">Market from which the data were gathered. Options are 'RT5M' for real-time 5 minute, 'RTHR' for real-time hourly, or 'DAHR' for day-ahead hourly. e.g., market=RT5M</param>
        /// <param name="offsetHours">Request size: 24 = 144 data points (24hr * 60min / 10min service fact intervals), 12 = 72 data points</param>
        /// <param name="offsetDays">Lenght of history, in days</param>
        /// <returns></returns>
        protected async Task<int> LoadAsync(string ba = "CAISO", int pageSize = 100, string freq = "10m", string market = "RT5M", int offsetHours = 12, int offsetDays = -1)
        {
            await this.ClearLastInterval();

            var now = DateTime.Now;
            DateTime start;
            DateTime end;

            var log = await _wattTimeV1aStadeStorage.GetLastLog();

            if (log == null)
            {
                start = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0, DateTimeKind.Utc).AddDays(offsetDays);
                end = start.AddHours(offsetHours);
            }
            else
            {
                start = log.end_at;
                end = start.AddHours(offsetHours);
            }

            const string v1 = "https://api.watttime.org:443";
            //const string v2 = "https://api2.watttime.org/v2";

            HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Token", "47e938b1abb8e33f0227a9ca3d41d3d438fe4dde");
            HttpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "47e938b1abb8e33f0227a9ca3d41d3d438fe4dde");

            HttpClient.BaseAddress = new Uri(v1);

            var query = HttpUtility.ParseQueryString(string.Empty);
            query.Add("ba", ba);
            query.Add("page_size", pageSize.ToString());
            query.Add("freq", freq);    // 1hr
            query.Add("market", market); // DAHR

            while (end <= new DateTime(now.Year, now.Month, now.Day + 1, 0, 0, 0, DateTimeKind.Utc))
            {
                var startAt = start.AddMinutes(5);
                var endAt = end;

                var currentLog = await _wattTimeV1aStadeStorage.GetLog(startAt: startAt, endAt: endAt);

                if (currentLog == null)
                {
                    var result = await this.LoadInterval(start: start.AddMinutes(5), end: end, ba: ba, query: query, pageSize: pageSize, market: market, freq: freq);
                    
                    // stop processing if error:
                    if (result != 0) return result;
                }

                start = start.AddHours(offsetHours);
                end = end.AddHours(offsetHours);
            }
            return 0;
        }

        /// <summary> Last interval have to be updated (deleted before processing) </summary>
        private async Task<bool> ClearLastInterval()
        {
            var lastLog = await _wattTimeV1aStadeStorage.GetLastLog();
            if (lastLog == null) return true;

            // clear log without points:
            while (lastLog?.count <= 0)
            {
                await _wattTimeV1aStadeStorage.DeleteLog(lastLog.LogId);
                lastLog = await _wattTimeV1aStadeStorage.GetLastLog();
            }

            // clear last log:
            lastLog = await _wattTimeV1aStadeStorage.GetLastLog();
            await _wattTimeV1aStadeStorage.DeleteLog(lastLog.LogId);

            return true;
        }

        private async Task<int> LoadInterval(DateTime start, DateTime end, string ba, NameValueCollection query, int pageSize, string freq, string market)
        {
            query["start_at"] = start.ToString("yyyy-MM-ddTHH:mm:00");
            query["end_at"] = end.ToString("yyyy-MM-ddTHH:mm:00");

            using (var httpResponseMessage = await HttpClient.GetAsync("/api/v1/datapoints/?" + query))
            {
                // check - no result
                if (httpResponseMessage?.Content == null)
                {
                    await _wattTimeV1aStadeStorage.AddLog(new Contracts.WattTimeV1.StageWattTimeV1ActualLog()
                    {
                        count = 0,
                        StatusCode = httpResponseMessage?.StatusCode.ToString(),
                        ReasonPhrase = $"{httpResponseMessage?.ReasonPhrase}, httpResponseMessage?.Content == null",
                        start_at = start, end_at = end, ba = ba, page_size = pageSize, freq = freq, market = market
                    });
                    return -1;
                }

                string content = await httpResponseMessage.Content.ReadAsStringAsync();

                // check - TOO MANY REQUESTS, Request was throttled...
                if (httpResponseMessage.StatusCode != HttpStatusCode.OK)
                {
                    JObject errorjObject = JObject.Parse(content);
                    string errorDetail = errorjObject["detail"]?.Value<string>();

                    Console.WriteLine($"{httpResponseMessage?.ReasonPhrase}. {errorDetail}");

                    await _wattTimeV1aStadeStorage.AddLog(new Contracts.WattTimeV1.StageWattTimeV1ActualLog
                    {
                        count = 0,
                        StatusCode = httpResponseMessage?.StatusCode.ToString(),
                        ReasonPhrase = $"{httpResponseMessage?.ReasonPhrase}. {errorDetail}",
                        start_at = start, end_at = end, ba = ba, page_size = pageSize, freq = freq, market = market
                    });
                    return (int)httpResponseMessage.StatusCode;
                }

                JObject jObject = JObject.Parse(content);

                int? count = jObject["count"]?.Value<int?>();
                string next = jObject["next"]?.Value<string>();

                var results = jObject["results"]?.Children().Select(item => new
                {
                    timestamp = item["timestamp"]?.Value<DateTime?>(),
                    created_at = item["created_at"]?.Value<DateTime?>(),
                    carbon = item["carbon"]?.Value<Decimal?>()
                });

                Console.WriteLine($"    {httpResponseMessage?.StatusCode}    {count}    {next}");
                Console.WriteLine($"    {query["start_at"]}  -  {query["end_at"]}");

                var log = new StageWattTimeV1ActualLog()
                {
                    count = count ?? 0,
                    StatusCode = httpResponseMessage?.StatusCode.ToString(),
                    ReasonPhrase = httpResponseMessage?.ReasonPhrase,
                    start_at = start, end_at = end, ba = ba, page_size = pageSize, freq = freq, market = market, next = next
                };

                long newLogId = await _wattTimeV1aStadeStorage.AddLog(log);

                if (results != null)
                    foreach (var item in results.OrderBy(i => i.created_at))
                    {
                        Console.WriteLine($"{item?.timestamp}    {item?.created_at}    {item?.carbon}");

                        if (item.created_at.HasValue && item.carbon.HasValue && newLogId != 0)
                        {
                            var point = new StageWattTimeV1Actual()
                            {
                                created_at = item.created_at.Value,
                                ba = ba,
                                carbon = item.carbon.Value,
                                LogId = newLogId,

                                _year = (Int16)item.created_at.Value.Year,
                                _quarter = (byte) ((item.created_at.Value.Month - 1) / 3 + 1),
                                _weekOfYear = (byte)(item.created_at.Value.DayOfYear / 7),
                                _month = (byte)item.created_at.Value.Month,
                                _day = (byte)item.created_at.Value.Day,
                                _dayOfWeek = (byte)item.created_at.Value.DayOfWeek,
                                _hour = (byte)item.created_at.Value.Hour,
                                _minute = (byte)item.created_at.Value.Minute
                            };
                            await _wattTimeV1aStadeStorage.AddPoint(point);
                        }
                    }
            }
            return 0;
        }
    }
}
