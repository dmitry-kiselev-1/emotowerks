﻿using System;
using System.Threading.Tasks;

namespace Emw.CarbonPredictions.ETL.Stage
{
    public class WattTimeV1a: WattTimeV1
    {
        private const string ba = "CAISO";
        private const int page_size = 100;
        private const string freq = "10m";
        private const string market = "RT5M";
        private const int offsetHours = 12;
        private const int offsetDays = -366;

        public async Task<int> LoadAsync()
        {
            return await base.LoadAsync(ba: ba, pageSize: page_size, freq: freq, market: market, offsetHours: offsetHours, offsetDays: offsetDays);
        }
    }
}
