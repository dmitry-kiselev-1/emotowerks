﻿using Emw.CarbonPredictions.Configuration;
using Emw.CarbonPredictions.Contracts;
using Emw.CarbonPredictions.Contracts.WattTimeV1;
using Ninject;

namespace Emw.CarbonPredictions.ETL
{
    public abstract class Etl
    {
        protected static readonly NinjectConfigurationModule NinjectConfigurationModule = new NinjectConfigurationModule();
        protected static readonly IKernel NinjectKernel = new StandardKernel(NinjectConfigurationModule);

        protected readonly ICarbonPredictionsStorage _carbonPredictionsStorage;
        protected readonly IWattTimeV1aStadeStorage _wattTimeV1aStadeStorage;

        protected Etl()
        {
            _carbonPredictionsStorage = NinjectKernel.Get<ICarbonPredictionsStorage>();
            _wattTimeV1aStadeStorage = NinjectKernel.Get<IWattTimeV1aStadeStorage>();
        }
    }
}
