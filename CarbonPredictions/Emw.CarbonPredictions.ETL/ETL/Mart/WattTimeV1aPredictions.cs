﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Emw.CarbonPredictions.Configuration;
using Emw.CarbonPredictions.Contracts;
using Emw.CarbonPredictions.Contracts.WattTimeV1;
using Emw.Common;
using Emw.Common.Extensions;
using Ninject;

namespace Emw.CarbonPredictions.ETL.Mart
{
    public class WattTimeV1aPredictions: Etl
    {
        private const int TimeGranularityMinute = 5;
        private const int ForecastDay = 2;
        private const int BatchSize = 1000;
        private const string DefaultBalancingAuthorities = "CAISO";

        public async Task<int> LoadAsync()
        {
            var stadeIsEmpty = await _wattTimeV1aStadeStorage.IsEmpty(DefaultBalancingAuthorities);
            if (stadeIsEmpty) return -1;

            var now = DateTime.Now;
            DateTime start = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0, DateTimeKind.Utc);
            DateTime end = start.AddDays(ForecastDay);
            List<DateTimeRange> interval;

            var martIsEmpty = await _carbonPredictionsStorage.IsEmpty();

            if (martIsEmpty)
            {
                interval = new DateTimeRange(start, end).Slice(new TimeSpan(hours: 0, minutes: 5, seconds: 0));
            }
            else
            {
                var clearFrom = now.Floor(new TimeSpan(0, 5, 0));
                await _carbonPredictionsStorage.RemovePointsInterval(clearFrom);

                await this.UpdateActuals(newOnly: false);

                var lastCarbonPrediction = await _carbonPredictionsStorage.GetLastAsync();
                start = lastCarbonPrediction.IntervalStartTime.AddMinutes(TimeGranularityMinute);
                var toEndTimeSpan = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0, DateTimeKind.Utc).AddDays(ForecastDay) - start;
                end = start.Add(toEndTimeSpan);
                interval = new DateTimeRange(start, end).Slice(new TimeSpan(hours: 0, minutes: 5, seconds: 0));
            }

            var martIntervals = await this.GetPointsInterval(interval);
            bool result = await _carbonPredictionsStorage.AddPointsInterval(martIntervals);
            return result ? 0 : 1;
        }

        private async Task<List<Contracts.CarbonPrediction>> GetPointsInterval(List<DateTimeRange> interval)
        {
            List<Contracts.CarbonPrediction> carbonPredictions = new List<Contracts.CarbonPrediction>();

            int n = 1;
            foreach (var i in interval.Take(BatchSize))
            {
                var stageIntervals = (await _wattTimeV1aStadeStorage.GetPointsByDayOfWeek(DefaultBalancingAuthorities, i.From, i.To));

                var stageIntervalsOrdered = stageIntervals.OrderBy(s => s.created_at)
                    .Select((s, index) => new { index = index + 1, carbon = s.carbon }).ToList();

                //var predictedAverage = stageIntervals.Average(s => s.carbon);
                var predictedWeightedAverage = stageIntervalsOrdered.WeightedAverage(
                    intervalOrdered => intervalOrdered.carbon,
                    weight => (decimal)Math.Log10(weight.index + 1));

                carbonPredictions.Add(new Contracts.CarbonPrediction()
                {
                    IntervalStartTime = i.From,
                    Predicted = predictedWeightedAverage
                });

                Console.WriteLine($"Add {n++} of {interval.Count}    {i.From}");
            }

            return carbonPredictions.Any() ? carbonPredictions : null;
        }

        private async Task<int> UpdateActuals(bool newOnly = true)
        {
            var first = newOnly 
                ? await _carbonPredictionsStorage.GetFirstEmptyActualAsync() 
                : await _carbonPredictionsStorage.GetFirstAsync();

            var last = await _carbonPredictionsStorage.GetLastAsync();

            var predictedIntervalsToUpdate = await _carbonPredictionsStorage.GetPredictedIntervalsAsync(string.Empty, first.IntervalStartTime, last.IntervalStartTime);

            foreach (var interval in predictedIntervalsToUpdate)
            {
                var stagePoint = await _wattTimeV1aStadeStorage.GetPoint(
                    DefaultBalancingAuthorities,
                    interval.IntervalStartTime, interval.IntervalStartTime.AddMinutes(TimeGranularityMinute));

                interval.Actual = stagePoint?.carbon;
            }

            await _carbonPredictionsStorage.UpdateActual(predictedIntervalsToUpdate);

            return await Task.Run(() => 0);
        }

    }
}
